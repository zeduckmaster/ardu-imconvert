A simple image converter for Arduboy which focus on real time display and format of converted data.

![ardu-imconvert](https://gitlab.com/zeduckmaster/ardu-imconvert/raw/master/screen.png "ardu-imconvert screenshot")

# Features

- Real time data visualization.
- Supports .png and .gif image format.
- Preview function.
- Animation playback.
- Simple raw data format display.
- Extracts mask data from alpha.
- Single or multiple source file export (in c/c++ header format).
- Support for RLE Cabi compression mode (compatible with Arduboy2::drawCompressed).
- Support for ArdBitmap compression mode (compatible with ArdBitmap library).
- Simple assets directory browser (allow fast switch between images in the same directory).
- Format data for multiple drawing functions:
  - Arduboy2::drawBitmap
  - Arduboy2::drawCompressed
  - Sprites::drawExternalMask
  - Sprites::drawOverwrite
  - Sprites::drawErase
  - Sprites::drawSelfMasked
  - ArdBitmap::drawCompressed
- Basic image operations:
  - Negative (invert black and white)
  - Slice tiling
  
# How to use

- You can zoom in and out and pan the current loaded image using mouse.
- Change format to display ready-to-use converted data for specified drawing function.
- Use the `Slice` function to split your image into tiles (useful for tilemap) or sprite frames.
- `Export` function uses the current loaded image and current selected format and create/update a c/c++ header file next to the image using the same base filename.
- `Export Selected` function exports all selected images from the directory browser using the same behaviour as the `Export` function.
- `Export All` function exports all images from the directory browser using the same behaviour as the `Export` function.
- `Export As` function(s) allow you to export one or multiple image to one single c/c++ header file.

# Binaries

Check the releases : https://gitlab.com/zeduckmaster/ardu-imconvert/-/releases

# How to build

You can build the application using the Qt Creator project (.pro file). It requires Qt 5 minimum.

# Related Links

- Arduboy2 library: https://github.com/MLXXXp/Arduboy2
- ArdBitmap library: https://github.com/igvina/ArdBitmap
- Icon used: http://www.visualpharm.com/

License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
