#pragma once

#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QDialog>
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QTimer>
#include <QTreeView>
#include <QFileSystemModel>
#include <QStackedWidget>
#include <memory>

enum eFormat : uint32_t
{
	None = 0,
	RLECabi,
	Arduboy2_drawBitmap,
	Arduboy2_drawCompressed,
	Sprites_drawExternalMask,
	Sprites_drawOverwrite,
	Sprites_drawErase,
	Sprites_drawSelfMasked,
	ArdBitmap_drawCompressed
};

struct SliceInfo
{
	int tileWidth = 0;
	int tileHeight = 0;
};

class ImageView : public QGraphicsView
{
	Q_OBJECT;

public:
	ImageView(QWidget* parent);
	virtual ~ImageView() override = default;
	virtual void updateImage(QImage const& image, QImage const& imagePreview, SliceInfo const& sliceInfo);
	inline void resetScale() { _setScale(6); }
	void showPreview(bool value);

signals:
	void imageDropped(QString const& filepath);

protected:
	virtual void wheelEvent(QWheelEvent* wheelEvent) override;
	virtual void dragEnterEvent(QDragEnterEvent* dragEnterEvent) override;
	virtual void dragMoveEvent(QDragMoveEvent* dragMoveEvent) override;
	virtual void dropEvent(QDropEvent* dropEvent) override;

private:
	void _setScale(int value);

	QGraphicsScene _scene;
	QGraphicsPixmapItem _imageItem;
	QGraphicsPixmapItem _imagePreviewItem;
	QGraphicsSimpleTextItem _textItem;
	int _scaleIdx = 6;
};

////////////////////////////////////////////////////////////////////////////////

class DialogSlice : public QDialog
{
	Q_OBJECT;

public:
	DialogSlice(QWidget* parent);
	virtual ~DialogSlice() = default;

public:
	SliceInfo info;
};

////////////////////////////////////////////////////////////////////////////////

class MovieControl : public QWidget
{
	Q_OBJECT;

public:
	MovieControl(QWidget* parent);
	virtual ~MovieControl() = default;
	void setFrameDelays(std::vector<int> delays);
	inline void retranslate() { _ui.retranslate(); }

signals:
	void frameChanged(int index);

private:
	struct _UI
	{
		QPushButton* playButton = nullptr;
		QSlider* timelineSlider = nullptr;

		void setup(QWidget* w);
		void retranslate();
	};
	_UI _ui;
	std::vector<int32_t> _delays;
	QTimer _timer;
};

////////////////////////////////////////////////////////////////////////////////

class WindowMain : public QMainWindow
{
	Q_OBJECT;

public:
	WindowMain();
	virtual ~WindowMain() = default;

private:
	struct _Frame
	{
		QImage img;
		QImage imgPreview;
		int delay = 0;
		std::vector<uint8_t> data;
		size_t noCompressDataSize = 0;
		std::vector<uint8_t> mask;
		size_t noCompressMaskSize = 0;

		_Frame() = default;
		_Frame(QImage const& img, QImage const& imgPreview, int delay)
			: img{img}
			, imgPreview{imgPreview}
			, delay{delay}
		{}
	};

	void _openImage(QString const& filepath, bool updateDirView);
	std::vector<_Frame> _readImage(QImageReader& imgReader) const;
	void _updateImage();
	void _convertImages(std::vector<_Frame>& frames);
	void _compressDatas(std::vector<_Frame>& frames);

	struct _ToString
	{
		QString data;
		QString mask;
	};
	_ToString _toString(std::vector<_Frame>& frames, size_t frameIdx, eFormat format, QString const& imgName);
	void _export(std::vector<QString> const& srcFilepaths, std::vector<QString> const& dstFilepaths);
	void _exportAs(std::vector<QString> const& srcFilepaths, QString const& dstFilepath);
	void _displayData();
	void _resetParams();

	struct _UI
	{
		QLabel* formatLabel = nullptr;
		QComboBox* formatCombo = nullptr;
		ImageView* imageView = nullptr;
		QCheckBox* imagePreviewCheckBox = nullptr;
		QTreeView* dirView = nullptr;
		QTabWidget* tabsData = nullptr;
		QPlainTextEdit* imageData = nullptr;
		QPlainTextEdit* imageMask = nullptr;
		MovieControl* movieControl = nullptr;
		QLabel* dataInfo = nullptr;
		QStatusBar* statusBar = nullptr;
		QPushButton* copyToClipboard = nullptr;

		QAction* actionOpenImage = nullptr;
		QAction* actionExport = nullptr;
		QAction* actionExportSelected = nullptr;
		QAction* actionExportAll = nullptr;
		QAction* actionExportAs = nullptr;
		QAction* actionExportSelectedAs = nullptr;
		QAction* actionExportAllAs = nullptr;
		QAction* actionExit = nullptr;
		QAction* actionImageNegative = nullptr;
		QAction* actionImageSlice = nullptr;
		QAction* actionImageSliceRemove = nullptr;
		QAction* actionAbout = nullptr;
		QMenu* menuFile = nullptr;
		QMenu* menuImage = nullptr;
		QMenu* menuHelp = nullptr;

		void setup(QMainWindow* w);
		void retranslate();
	};
	_UI _ui;
	QFileSystemModel* _dirModel = nullptr;
	std::vector<_Frame> _curFrames;
	size_t _curFrameIdx = 0;
	QString _filepath;
	SliceInfo _imageSlice;
	bool _imageNegative = false;
};
