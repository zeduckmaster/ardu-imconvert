#include "window_main.h"
#include <QWheelEvent>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QMenuBar>
#include <QStyleOptionSlider>
#include <QImageReader>
#include <QMimeData>
#include <QMessageBox>
#include <QStatusBar>
#include <QSplitter>
#include <QLineEdit>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QTextStream>
#include <QDebug>
#include <QClipboard>
#include <QApplication>
#include <array>
#include <cmath>

namespace
{
	template<typename T>
	inline T clamp(T value, T min, T max) { return std::min<T>(std::max<T>(value, min), max); }

	auto const kScaleValues = std::array<float, 11>
	{{
		0.51f, 0.71f, 1.0f, 1.42f, 2.0f, 2.83f, 4.0f, 5.66f, 8.0f, 11.32f, 16.0f
	}};

	QString toHexString(uint8_t value)
	{
		auto nm = QString::number(value, 16);
		return QString{(nm.size() == 1)? "0x0" : "0x"} + nm;
	}

	auto const kMessageTimeout = 2000;

	////////////////////////////////////////////////////////////////////////////

	class RLE
	{
	public:
		std::vector<uint8_t> compress(std::vector<uint8_t> const& src, int width, int height)
		{
			_src = src;
			_dst = std::vector<uint8_t>{};
			_bit = 1;
			_putval(_getcol(0), 1); // first colour
			auto pos = 0;
			// span data
			auto nb = width * height;
			while(pos < nb)
			{
				auto rlen = _findrlen(pos, nb);
				pos += rlen;
				_putsplen(rlen-1);
			}
			// pad with zeros and flush
			while(_bit != 0x1)
				_putbit(0);
			return _dst;
		}
	private:
		uint32_t _byte = 0;
		uint32_t _bit = 0;
		std::vector<uint8_t> _src;
		std::vector<uint8_t> _dst;

		int _getcol(int pos)
		{
			if(_src[static_cast<size_t>(pos / 8)] & (1 << (pos & 7)))
				return 1;
			return 0;
		}

		int _findrlen(int pos, int plen)
		{
			auto col = _getcol(pos);
			auto pos0 = pos;
			while((pos < plen) && (_getcol(pos) == col))
				++pos;
			return pos - pos0;
		}

		void _putbit(int val)
		{
			if(val)
				_byte |= _bit;
			_bit <<= 1;
			if(_bit == 0x100)
			{
				_dst.push_back(static_cast<uint8_t>(_byte));
				_bit = 0x1;
				_byte = 0;
			}
		}

		void _putval(int val, int bits)
		{
			if(bits <= 0)
				return;
			for(auto i = 0; i < bits; ++i)
				_putbit(val & (1 << i));
		}

		void _putsplen(int len)
		{
			auto blen = 1; // how bits needed to encode length
			while((1 << blen) <= len)
				blen += 2;
			// write number of bits (1-terminated string of zeroes)
			_putval(0,(blen - 1) / 2);
			_putval(1, 1); // terminator
			// write length
			_putval(len, blen);
		}
	};

	////////////////////////////////////////////////////////////////////////////
	// code based on the java from: https://github.com/igvina/ArdBitmap

	class ArdBitmap
	{
	public:
		std::vector<uint8_t> compress(std::vector<uint8_t> const& src, int width, int height)
		{
			auto dst0 = _getEncodedImage(src, width, height, false, false);
			auto dst1 = _getEncodedImage(src, width, height, true, false);
			auto dst2 = _getEncodedImage(src, width, height, false, true);
			auto dst3 = _getEncodedImage(src, width, height, true, true);

			auto dst0sz = dst0.size();
			auto dst1sz = dst1.size();
			auto dst2sz = dst2.size();
			auto dst3sz = dst3.size();
			if((dst0sz < dst1sz) && (dst0sz < dst2sz) && (dst0sz < dst3sz))
				return dst0;
			if((dst1sz < dst0sz) && (dst1sz < dst2sz) && (dst1sz < dst3sz))
				return dst1;
			if((dst2sz < dst0sz) && (dst2sz < dst1sz) && (dst2sz < dst3sz))
				return dst2;
			return dst3;
		}
	private:
		std::vector<uint8_t> _getEncodedImage(std::vector<uint8_t> const& src, int width, int height, bool zigzag, bool inverted)
		{
			auto dst = std::vector<uint8_t>{};
			auto character = 0;
			auto characterPos = 0;
			auto counter = 0;
			auto bit = 0;
			auto size = src.size() * 8;
			for(auto i = size_t{0}; i < size; ++i)
			{
				auto mode = (zigzag == true)? (((i/8)%static_cast<uint32_t>(width))%2) == 0 : true;
				if(inverted == true)
					mode = !mode;
				auto actualBit = (src[i/8] >> (mode? i%8 : 7 - i%8)) & 0x01;
				if(counter == 0)
				{
					bit = actualBit;
					if (i == 0)
					{
						dst.push_back(static_cast<uint8_t>((width - 1) | (bit << 7)));
						dst.push_back(static_cast<uint8_t>((height - 1)  | ((zigzag ? 1 :0) << 7)  | ((inverted ? 1 :0) << 6)));
					}
				}
				if(actualBit == bit)
					++counter;
				else
				{
					auto numberBits = _getNumberOfBits(counter);
					for(auto n = int{0}; n < (numberBits - 1); ++n)
					{
						character |= (0x01 << characterPos);
						++characterPos;
						if(characterPos == 8)
						{
							dst.push_back(static_cast<uint8_t>(character));
							character = 0;
							characterPos = 0;
						}
					}
					++characterPos;
					if(characterPos == 8)
					{
						dst.push_back(static_cast<uint8_t>(character));
						character = 0;
						characterPos = 0;
					}
					auto counterMod = static_cast<int>(counter - 1 - std::pow(2, numberBits  - 1));
					for(auto n = int{0}; n < (numberBits - 1); ++n)
					{
						character |= (((counterMod >> n)& 0x01) << characterPos);
						++characterPos;
						if(characterPos == 8)
						{
							dst.push_back(static_cast<uint8_t>(character));
							character = 0;
							characterPos = 0;
						}
					}
					if(numberBits == 1)
					{
						character |= (((counter >> 1)& 0x01) << characterPos);
						++characterPos;
						if (characterPos == 8)
						{
							dst.push_back(static_cast<uint8_t>(character));
							character = 0;
							characterPos = 0;
						}
					}
					counter = 1;
					bit = actualBit;
				}
			}
			if(counter > 0)
			{
				auto numberBits = _getNumberOfBits(counter);
				for(auto n = int{0}; n < (numberBits - 1); ++n)
				{
					character |= (0x01 << characterPos);
					++characterPos;
					if(characterPos == 8)
					{
						dst.push_back(static_cast<uint8_t>(character));
						character = 0;
						characterPos = 0;
					}
				}
				++characterPos;
				if(characterPos == 8)
				{
					dst.push_back(static_cast<uint8_t>(character));
					character = 0;
					characterPos = 0;
				}
				auto counterMod = static_cast<int>(counter - 1 - std::pow(2, numberBits  - 1));
				for(auto n = int{0}; n < (numberBits - 1); ++n)
				{
					character |= (((counterMod >> n)& 0x01) << characterPos);
					++characterPos;
					if((characterPos == 8) || (n == (numberBits - 2)))
					{
						dst.push_back(static_cast<uint8_t>(character));
						character = 0;
						characterPos = 0;
					}
				}
				if(numberBits == 1)
				{
					character |= (((counter >> 1)& 0x01) << characterPos);
					++characterPos;
					dst.push_back(static_cast<uint8_t>(character));
					character = 0;
					characterPos = 0;
				}
			}
			return dst;
		}

		int _getNumberOfBits(int number)
		{
			auto val = 1;
			while(std::pow(2, val) < number)
				++val;
			return val;
		}
	};
}

ImageView::ImageView(QWidget* parent)
	: QGraphicsView{parent}
{
	setCacheMode(QGraphicsView::CacheNone);
	setAcceptDrops(true);
	_scene.setBackgroundBrush(Qt::gray);
	_imageItem.setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
	_scene.addItem(&_imageItem);
	_imagePreviewItem.setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
	_imagePreviewItem.hide();
	_scene.addItem(&_imagePreviewItem);
	_textItem.setText(tr("Right-click or\ndrag and drop\nto open image..."));
	auto font = _textItem.font();
	font.setPointSize(14);
	_textItem.setFont(font);
	_scene.addItem(&_textItem);
	setScene(&_scene);
	setDragMode(QGraphicsView::ScrollHandDrag);
}

void ImageView::updateImage(QImage const& image, QImage const& imagePreview, SliceInfo const& sliceInfo)
{
	Q_UNUSED(sliceInfo);
	_textItem.hide();
	_scene.setSceneRect(0, 0, image.width(), image.height());
	_imageItem.setPixmap(QPixmap::fromImage(image));
	_imagePreviewItem.setPixmap(QPixmap::fromImage(imagePreview));
	auto rectf = _imageItem.boundingRect();
	_imageItem.setPos((_scene.width() - rectf.width()) * 0.5, (_scene.height() - rectf.height()) * 0.5);
	_imagePreviewItem.setPos((_scene.width() - rectf.width()) * 0.5, (_scene.height() - rectf.height()) * 0.5);
}

void ImageView::showPreview(bool value)
{
	_imageItem.setVisible(!value);
	_imagePreviewItem.setVisible(value);
}

void ImageView::wheelEvent(QWheelEvent* wheelEvent)
{
	_setScale(_scaleIdx + ((wheelEvent->delta() > 0)? 1 : -1));
}

void ImageView::dragEnterEvent(QDragEnterEvent* dragEnterEvent)
{
	auto mimeData = dragEnterEvent->mimeData();
	if(mimeData->hasUrls() == true)
		dragEnterEvent->accept();
	else
		QGraphicsView::dragEnterEvent(dragEnterEvent);
}

void ImageView::dragMoveEvent(QDragMoveEvent* dragMoveEvent)
{
	auto mimeData = dragMoveEvent->mimeData();
	if(mimeData->hasUrls() == true)
		dragMoveEvent->accept();
	else
		QGraphicsView::dragMoveEvent(dragMoveEvent);
}

void ImageView::dropEvent(QDropEvent* dropEvent)
{
	auto mimeData = dropEvent->mimeData();
	emit imageDropped(mimeData->urls().first().toLocalFile());
	QGraphicsView::dropEvent(dropEvent);
}

void ImageView::_setScale(int value)
{
	_scaleIdx = clamp<int>(value, 0, static_cast<int>(kScaleValues.size()-1));
	auto scale = kScaleValues[static_cast<size_t>(_scaleIdx)];
	auto tf = QTransform::fromScale(static_cast<qreal>(scale), static_cast<qreal>(scale));
	setTransform(tf);
}

////////////////////////////////////////////////////////////////////////////////

void MovieControl::_UI::setup(QWidget *w)
{
	auto layout = new QHBoxLayout{w};
	layout->setMargin(0);

	// play
	playButton = new QPushButton{w};
	layout->addWidget(playButton);
	playButton->setIcon(w->style()->standardIcon(QStyle::SP_MediaPlay));
	playButton->setIconSize(QSize{24, 24});
	playButton->setMaximumWidth(32);

	// timeline
	timelineSlider = new QSlider{Qt::Horizontal, w};
	layout->addWidget(timelineSlider);
	timelineSlider->setTickPosition(QSlider::TicksBelow);
	timelineSlider->setTickInterval(1);
	timelineSlider->setMaximum(0);
}

void MovieControl::_UI::retranslate()
{
}

MovieControl::MovieControl(QWidget* parent)
	: QWidget{parent}
{
	_ui.setup(this);
	_ui.retranslate();

	connect(_ui.timelineSlider, &QSlider::valueChanged, [&](int index) { emit frameChanged(index); } );
	connect(_ui.playButton, &QPushButton::clicked,
		[&]()
	{
		if(_timer.isActive() == true)
		{
			_timer.stop();
			_ui.playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
			return;
		}
		auto curFrame = _ui.timelineSlider->value();
		_timer.start(_delays[static_cast<size_t>(curFrame)]);
		_ui.playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
	});
	connect(&_timer, &QTimer::timeout,
		[&]()
	{
		auto newFrame = _ui.timelineSlider->value() + 1;
		if(newFrame >= static_cast<int>(_delays.size()))
			newFrame = 0;
		_ui.timelineSlider->setValue(newFrame);
		_timer.start(_delays[static_cast<size_t>(newFrame)]);
	});
}

void MovieControl::setFrameDelays(std::vector<int> delays)
{
	_delays = delays;
	setEnabled(_delays.size() > 1);
	auto sl = _ui.timelineSlider;
	auto bs = sl->blockSignals(true);
	sl->setValue(0);
	sl->setMinimum(0);
	sl->setMaximum(static_cast<int>(_delays.size()) - 1);
	sl->blockSignals(bs);
}

////////////////////////////////////////////////////////////////////////////////

DialogSlice::DialogSlice(QWidget* parent)
	: QDialog{parent}
{
	setMinimumWidth(250);
	setWindowTitle(tr("Slice Image"));
	auto layout = new QVBoxLayout{this};
	setLayout(layout);

	{
		auto qw = new QWidget{this};
		auto ql = new QFormLayout{qw};
		layout->addWidget(qw);
		ql->setMargin(0);

		auto tileWidth = new QLineEdit{qw};
		tileWidth->setMaximumWidth(50);
		tileWidth->setValidator(new QIntValidator{0, 1024, qw});
		ql->addRow(tr("Tile Width"), tileWidth);
		connect(tileWidth, &QLineEdit::textEdited, [=](QString const& value) { info.tileWidth = value.toInt(); });

		auto tileHeight = new QLineEdit{qw};
		tileHeight->setMaximumWidth(50);
		tileHeight->setValidator(new QIntValidator{0, 1024, qw});
		ql->addRow(tr("Tile Height"), tileHeight);
		connect(tileHeight, &QLineEdit::textEdited, [=](QString const& value) { info.tileHeight = value.toInt(); });
	}
	{
		auto buttonBox = new QDialogButtonBox{this};
		auto buttonCancel = buttonBox->addButton(tr("Cancel"), QDialogButtonBox::RejectRole);
		connect(buttonCancel, &QPushButton::clicked, this, &QDialog::reject);
		auto buttonAccept = buttonBox->addButton(tr("Apply"), QDialogButtonBox::AcceptRole);
		connect(buttonAccept, &QPushButton::clicked, this, &QDialog::accept);
		layout->addWidget(buttonBox);
	}
}

////////////////////////////////////////////////////////////////////////////////

void WindowMain::_UI::setup(QMainWindow* w)
{
	Q_ASSERT(w != nullptr);
	w->resize(600, 480);
	w->setMinimumSize(600, 480);
	auto cw = new QWidget{w};
	w->setCentralWidget(cw);
	auto layout = new QVBoxLayout{cw};

	// left/right
	{
		auto qw0 = new QWidget{w};
		layout->addWidget(qw0, 1);
		auto ql0 = new QHBoxLayout{qw0};
		ql0->setMargin(0);

		// left
		{
			auto qs1 = new QSplitter{Qt::Vertical, w};
			qs1->setChildrenCollapsible(false);
			ql0->addWidget(qs1);
			// dir browser
			dirView = new QTreeView{w};
			dirView->setSelectionMode(QAbstractItemView::ExtendedSelection);
			qs1->addWidget(dirView);
			// image view
			imageView = new ImageView{w};
			imageView->setContextMenuPolicy(Qt::CustomContextMenu);
			qs1->addWidget(imageView);
			qs1->setSizes(QList<int>{} << 150 << 250);
			// image preview checkbox
			imagePreviewCheckBox = new QCheckBox{w};
			qs1->addWidget(imagePreviewCheckBox);
		}

		// right
		{
			auto qw1 = new QWidget{w};
			qw1->setMinimumWidth(380);
			qw1->setMaximumWidth(380);
			ql0->addWidget(qw1);
			auto ql1 = new QVBoxLayout{qw1};
			ql1->setMargin(0);
			// format
			{
				auto qw2 = new QWidget{w};
				ql1->addWidget(qw2);
				auto ql2 = new QHBoxLayout{qw2};
				ql2->setMargin(0);
				formatLabel = new QLabel{w};
				ql2->addWidget(formatLabel);
				formatCombo = new QComboBox{w};
				ql2->addWidget(formatCombo);
				ql2->addStretch();
				copyToClipboard = new QPushButton{w};
				copyToClipboard->setIcon(QIcon{QStringLiteral(":/icons/icon_copy.png")});
				ql2->addWidget(copyToClipboard);
			}
			// tabs data
			tabsData = new QTabWidget{w};
			tabsData->setTabPosition(QTabWidget::North);
			ql1->addWidget(tabsData);
			// image data
			imageData = new QPlainTextEdit{w};
			imageData->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
			tabsData->addTab(imageData, "");
			// image mask
			imageMask = new QPlainTextEdit{w};
			imageMask->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
			tabsData->addTab(imageMask, "");
		}
	}

	// movie control
	movieControl = new MovieControl{w};
	movieControl->setEnabled(false);
	layout->addWidget(movieControl);

	// status bar
	{
		statusBar = new QStatusBar{w};
		statusBar->setSizeGripEnabled(true);
		w->setStatusBar(statusBar);

		// data info
		dataInfo = new QLabel{w};
		statusBar->addPermanentWidget(dataInfo);
	}

	actionOpenImage = new QAction{w};
	actionOpenImage->setShortcut(QKeySequence{Qt::CTRL + Qt::Key_O});
	actionExport = new QAction{w};
	actionExport->setShortcut(QKeySequence{Qt::CTRL + Qt::Key_E});
	actionExportSelected = new QAction{w};
	//actionExportSelected->setShortcut(QKeySequence{Qt::CTRL + Qt::SHIFT + Qt::Key_E});
	actionExportAll = new QAction{w};
	actionExportAs = new QAction{w};
	actionExportSelectedAs = new QAction{nullptr};
	actionExportAllAs = new QAction{nullptr};
	actionExit = new QAction{w};
	actionExit->setMenuRole(QAction::QuitRole);
	actionExit->setShortcut(QKeySequence{Qt::ALT + Qt::Key_F4});
	actionImageNegative = new QAction{w};
	actionImageSlice = new QAction{w};
	actionImageSliceRemove = new QAction{w};
	actionAbout = new QAction{w};
	actionAbout->setMenuRole(QAction::AboutRole);

	// menu
	{
		auto menubar = new QMenuBar{};
		w->setMenuBar(menubar);
		menuImage = new QMenu{menubar};
		menuImage->addAction(actionImageNegative);
		menuImage->addSeparator();
		menuImage->addAction(actionImageSlice);
		menuImage->addAction(actionImageSliceRemove);

		menuFile = new QMenu{menubar};
		menuFile->addAction(actionOpenImage);
		menuFile->addSeparator();
		menuFile->addAction(actionExport);
		menuFile->addAction(actionExportSelected);
		menuFile->addAction(actionExportAll);
		menuFile->addSeparator();
		menuFile->addAction(actionExportAs);
		menuFile->addAction(actionExportSelectedAs);
		menuFile->addAction(actionExportAllAs);
		menuFile->addSeparator();
		menuFile->addAction(actionExit);
		menuHelp = new QMenu{menubar};
		menuHelp->addAction(actionAbout);
		menubar->addMenu(menuFile);
		menubar->addMenu(menuImage);
		menubar->addMenu(menuHelp);
	}
}

void WindowMain::_UI::retranslate()
{
	imagePreviewCheckBox->setText(tr("Show Preview"));
	formatLabel->setText(tr("Format"));
	auto ql = QStringList{};
	ql << tr("None");
	ql << QStringLiteral("RLE (Cabi)");
	ql << QStringLiteral("Arduboy2::drawBitmap");
	ql << QStringLiteral("Arduboy2::drawCompressed");
	ql << QStringLiteral("Sprites::drawExternalMask");
	ql << QStringLiteral("Sprites::drawOverwrite");
	ql << QStringLiteral("Sprites::drawErase");
	ql << QStringLiteral("Sprites::drawSelfMasked");
	ql << QStringLiteral("ArdBitmap::drawCompressed");
	formatCombo->addItems(ql);
	copyToClipboard->setText(tr("Copy to Clipboard"));
	tabsData->setTabText(0, tr("Data"));
	tabsData->setTabText(1, tr("Mask"));
	movieControl->retranslate();

	actionOpenImage->setText(tr("Open Image..."));
	actionExport->setText(tr("Export"));
	actionExportSelected->setText(tr("Export Selected"));
	actionExportAll->setText(tr("Export All"));
	actionExportAs->setText(tr("Export As..."));
	actionExportSelectedAs->setText(tr("Export Selected As..."));
	actionExportAllAs->setText(tr("Export All As..."));
	actionExit->setText(tr("Exit"));
	actionImageNegative->setText(tr("Negative"));
	actionImageSlice->setText(tr("Slice..."));
	actionImageSliceRemove->setText(tr("Remove Slice"));
	actionAbout->setText(tr("About ardu-imconvert"));

	menuFile->setTitle(tr("File"));
	menuImage->setTitle(tr("Image"));
	menuHelp->setTitle(tr("Help"));
}

WindowMain::WindowMain()
	: QMainWindow{}
{
	setWindowTitle(QStringLiteral("ardu-imconvert"));
	_ui.setup(this);
	_ui.retranslate();

	connect(_ui.actionExit, &QAction::triggered, [&]() { close(); });
	connect(_ui.actionOpenImage, &QAction::triggered, [&]()
	{
		auto qfilepath = QFileDialog::getOpenFileName(this, tr("Select Image"), "", tr("Image files (*.png *.gif)"));
		if(qfilepath.isEmpty() == true)
			return;
		_resetParams();
		_openImage(qfilepath, true);
	});
	connect(_ui.actionExport, &QAction::triggered, [&]()
	{
		auto fi = QFileInfo{_filepath};
		_export({_filepath}, {fi.path() + QStringLiteral("/") + fi.baseName() + QStringLiteral(".h")});
		_ui.statusBar->showMessage("\"" + fi.fileName() + "\" exported", kMessageTimeout);
	});
	connect(_ui.actionExportSelected, &QAction::triggered, [&]()
	{
		if(_ui.dirView->selectionModel() == nullptr)
			return;
		auto indexes = _ui.dirView->selectionModel()->selectedIndexes();
		if(indexes.size() == 0)
		{
			QMessageBox::critical(this, tr("Error"), tr("No image selected!"));
			return;
		}
		auto srcs = std::vector<QString>{};
		srcs.reserve(static_cast<size_t>(indexes.size()));
		auto dsts = std::vector<QString>{};
		dsts.reserve(static_cast<size_t>(indexes.size()));
		for(auto& index : indexes)
		{
			if(index.column() != 0)
				continue;
			auto fi = _dirModel->fileInfo(index);
			srcs.emplace_back(fi.filePath());
			dsts.emplace_back(fi.path() + QStringLiteral("/") + fi.baseName() + QStringLiteral(".h"));
		}
		_export(srcs, dsts);
	});
	connect(_ui.actionExportAll, &QAction::triggered, [&]()
	{
		_ui.dirView->selectAll();
		_ui.actionExportSelected->trigger();
		_ui.dirView->selectionModel()->clearSelection();
	});
	connect(_ui.actionExportAs, &QAction::triggered, [&]()
	{
		auto dstFilepath = QFileDialog::getSaveFileName(this, tr("Export As"), "", tr("C/C++ header file (*.h)"));
		if(dstFilepath.isEmpty() == true)
			return;
		_exportAs({_filepath}, {dstFilepath});
	});
	connect(_ui.actionExportSelectedAs, &QAction::triggered, [&]()
	{
		if(_ui.dirView->selectionModel() == nullptr)
			return;
		auto indexes = _ui.dirView->selectionModel()->selectedIndexes();
		if(indexes.size() == 0)
		{
			QMessageBox::critical(this, tr("Error"), tr("No image selected!"));
			return;
		}
		auto dstFilepath = QFileDialog::getSaveFileName(this, tr("Export As"), "", tr("C/C++ header file (*.h)"));
		if(dstFilepath.isEmpty() == true)
			return;
		auto srcs = std::vector<QString>{};
		srcs.reserve(static_cast<size_t>(indexes.size()));
		for(auto& index : indexes)
		{
			if(index.column() != 0)
				continue;
			srcs.emplace_back(_dirModel->fileInfo(index).filePath());
		}
		_exportAs(srcs, dstFilepath);
	});
	connect(_ui.actionExportAllAs, &QAction::triggered, [&]()
	{
		_ui.dirView->selectAll();
		_ui.actionExportSelectedAs->trigger();
		_ui.dirView->selectionModel()->clearSelection();
	});
	connect(_ui.actionImageNegative, &QAction::triggered, [&]()
	{
		if(_filepath.isEmpty() == true)
			return;
		_imageNegative = !_imageNegative;
		_openImage(_filepath, false);
	});
	connect(_ui.actionImageSlice, &QAction::triggered, [&]()
	{
		if(_filepath.isEmpty() == true)
			return;
		DialogSlice dlg{nullptr};
		if(dlg.exec() != QDialog::Accepted)
			return;
		_imageSlice = dlg.info;
		_openImage(_filepath, false);
	});
	connect(_ui.actionImageSliceRemove, &QAction::triggered, [&]()
	{
		if(_filepath.isEmpty() == true)
			return;
		_imageSlice = SliceInfo{};
		_openImage(_filepath, false);
	});
	connect(_ui.actionAbout, &QAction::triggered, [&]()
	{
		QMessageBox mb{};
		mb.setIcon(QMessageBox::Information);
		mb.setMinimumSize(300, 200);
		mb.setWindowTitle(tr("About"));
		mb.setTextFormat(Qt::RichText);
		mb.setText(QStringLiteral("ardu-imconvert v0.9<br/><br/><a href=\"https://framagit.org/zeduckmaster/ardu-imconvert\">https://framagit.org/zeduckmaster/ardu-imconvert</a>"));
		mb.exec();
	});
	connect(_ui.imageView, &ImageView::customContextMenuRequested, [&](QPoint const& pos)
	{
		Q_UNUSED(pos);
		_ui.actionOpenImage->trigger();
	});
	connect(_ui.imageView, &ImageView::imageDropped, [&](QString const& filepath)
	{
		_imageNegative = false;
		_ui.imageView->resetScale();
		_openImage(filepath, true);
	});
	connect(_ui.imagePreviewCheckBox, &QCheckBox::stateChanged, [&](int state)
	{
		_ui.imageView->showPreview(state == Qt::Checked);
	});
	connect(_ui.formatCombo, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index)
	{
		if(_curFrames.size() == 0)
			return;
		auto hasMask = false;
		if((index == None) || (index == Sprites_drawExternalMask))
			hasMask = true;
		_ui.tabsData->setTabEnabled(1, hasMask);
		_convertImages(_curFrames);
		_compressDatas(_curFrames);
		_displayData();
	});
	connect(_ui.movieControl, &MovieControl::frameChanged, [&](int index)
	{
		_curFrameIdx = static_cast<size_t>(index);
		_updateImage();
	});
	connect(_ui.dirView, &QAbstractItemView::activated, [=](QModelIndex const& index)
	{
		_resetParams();
		_openImage(_dirModel->filePath(index), false);
	});
	connect(_ui.copyToClipboard, &QPushButton::clicked, [&]()
	{
		auto str = QString{};
		if(_ui.tabsData->currentIndex() == 0)
			str = _ui.imageData->toPlainText();
		else
			str = _ui.imageMask->toPlainText();
		QApplication::clipboard()->setText(str);
	});
}

void WindowMain::_openImage(QString const& filepath, bool updateDirView)
{
	QImageReader ir{filepath};
	if(ir.canRead() == false)
	{
		QMessageBox::critical(this, tr("Image Invalid"), tr("Invalid or not an image file!"));
		return;
	}
	// update tree view
	if(_dirModel == nullptr)
	{
		_dirModel = new QFileSystemModel{this};
		_dirModel->setNameFilterDisables(false);
		_dirModel->setNameFilters(QStringList{} << QStringLiteral("*.png") << QStringLiteral("*.gif"));
		_dirModel->setFilter(QDir::Files|QDir::AllDirs|QDir::NoDotAndDotDot);
		_ui.dirView->setModel(_dirModel);
		_ui.dirView->setHeaderHidden(true);
		_ui.dirView->hideColumn(1);
		_ui.dirView->hideColumn(2);
		_ui.dirView->hideColumn(3);
	}
	_filepath = filepath;
	auto fileInfo = QFileInfo{_filepath};
	if(updateDirView == true)
	{
		auto dir = fileInfo.absoluteDir().path();
		_dirModel->setRootPath(dir);
		_ui.dirView->setRootIndex(_dirModel->index(dir));
	}

	// update window title
	setWindowTitle(fileInfo.fileName() + QStringLiteral(" - ardu-imconvert"));

	// update image
	_curFrameIdx = 0;
	_curFrames = _readImage(ir);
	// handle slices
	if((_curFrames.size() == 1) && (_imageSlice.tileWidth > 0) && (_imageSlice.tileHeight > 0))
	{
		auto f0 = _curFrames[0];
		_curFrames.clear();
		auto tw = _imageSlice.tileWidth;
		auto th = _imageSlice.tileHeight;
		auto w = f0.img.width();
		auto h = f0.img.height();
		auto n = 0;
		for(auto y = 0; y < h; y += th)
		{
			for(auto x = 0; x < w; x += tw)
			{
				auto img = f0.img.copy(x, y, tw, th);
				auto imgPreview = f0.img.copy(x, y, tw, th);
				_curFrames.push_back(_Frame{img, imgPreview, n++});
			}
		}
	}
	auto delays = std::vector<int32_t>{};
	for(auto& frm : _curFrames)
		delays.push_back(frm.delay);
	_ui.movieControl->setFrameDelays(delays);

	_convertImages(_curFrames);
	_compressDatas(_curFrames);
	_updateImage();
	_ui.statusBar->showMessage("\"" + QFileInfo{_filepath}.fileName() + "\" opened", kMessageTimeout);
}

std::vector<WindowMain::_Frame> WindowMain::_readImage(QImageReader& imgReader) const
{
	// update image
	auto nb = imgReader.imageCount();
	if(nb == -1)
		nb = 1;
	auto frames = std::vector<_Frame>{};
	frames.reserve(static_cast<size_t>(nb));
	for(auto n = 0; n < nb; ++n)
	{
		auto img = imgReader.read();
		if(img.isNull()== false)
		{
			img = img.convertToFormat(QImage::Format_ARGB32);
			if(_imageNegative == true)
				img.invertPixels();
		}
		auto imgPreview = QImage{img.width(), img.height(), QImage::Format_ARGB32};
		auto delay = imgReader.nextImageDelay();
		frames.push_back(_Frame{img, imgPreview, delay});
	}
	return frames;
}

void WindowMain::_updateImage()
{
	auto& frame = _curFrames[_curFrameIdx];
	_ui.imageView->updateImage(frame.img, frame.imgPreview, _imageSlice);
	_displayData();
}

void WindowMain::_convertImages(std::vector<_Frame>& frames)
{
	// basic convert
	for(auto& frm : frames)
	{
		auto& img = frm.img;
		auto& imgPreview = frm.imgPreview;
		auto size = img.size();
		auto w = size.width();
		auto h = size.height();
		frm.data = std::vector<uint8_t>{};
		frm.mask = std::vector<uint8_t>{};
		auto nb = w * (h/8 + ((h%8)? 1: 0));
		if(h < 8)
			nb = w;
		frm.data.resize(static_cast<size_t>(nb));
		frm.mask.resize(static_cast<size_t>(nb));
		auto funcIsBlack = [](QColor const& value) { return (value.rgb() == 0xff000000); };
		auto funcIsTrans = [](QColor const& value) { return (value.alpha() == 0); };
		auto funcPreview = [=](QColor const& value)
		{
			auto r = (funcIsBlack(value) == true)? 0 : 255;
			return QColor{r, r, r, value.alpha()};
		};
		for(auto y = 0; y < h; y += 8)
		{
			for(auto x = 0; x < w; ++x)
			{
				auto pos = static_cast<size_t>(x + (y / 8) * w);
				auto& ydata = frm.data[pos];
				auto& ymask = frm.mask[pos];
				ydata = 0;
				ymask = 0;
				for(auto yy = 0; yy < 8; ++yy)
				{
					if((y + yy) >= h)
						break;
					auto color = img.pixelColor(x, y + yy);
					imgPreview.setPixelColor(x, y + yy, funcPreview(color));
					auto isTrans = funcIsTrans(color);
					ydata |= (funcIsBlack(color) || isTrans)? 0 : 1 << yy;
					ymask |= isTrans? 0 : 1 << yy;
				}
			}
		}
		frm.noCompressDataSize = frm.data.size();
		frm.noCompressMaskSize = frm.mask.size();
	}
}

void WindowMain::_compressDatas(std::vector<_Frame>& frames)
{
	auto idx = _ui.formatCombo->currentIndex();
	if(idx == 0)
		return;
	for(auto& frm : frames)
	{
		auto sz = frm.img.size();
		switch(idx)
		{
		case Arduboy2_drawCompressed:
			if((sz.width() > 255) || (sz.height() > 255))
			{
				frm.data.clear();
				break;
			}
		case RLECabi:
			frm.data = RLE{}.compress(frm.data, sz.width(), sz.height());
			break;
		case ArdBitmap_drawCompressed:
			if((sz.width() > 128) || (sz.height() > 64))
				break;
			frm.data = ArdBitmap{}.compress(frm.data, sz.width(), sz.height());
			break;
		}
	}
}

WindowMain::_ToString WindowMain::_toString(std::vector<_Frame>& frames, size_t frameIdx, eFormat format, QString const& imgName)
{
	auto funcWriteData = [=](QString& out, std::vector<uint8_t> const& data, bool hasTab)
	{
		auto str = QString{};
		if(hasTab == true)
			str += QStringLiteral("  ");
		auto nb = static_cast<int32_t>(data.size());
		for(auto n = 0; n < nb; ++n)
		{
			str += toHexString(data[static_cast<size_t>(n)]) + QStringLiteral(", ");
			if(((n + 1) % 8) == 0)
			{
				out += str + QStringLiteral("\n");
				str = QString{};
				if(hasTab == true)
					str += QStringLiteral("  ");
			}
		}
		if(str.size() > 2)
			out += str + QStringLiteral("\n");
	};

	auto& frm = frames[frameIdx];
	auto sz = frm.img.size();
	auto ts = _ToString{};
	switch(format)
	{
	case None:
	case RLECabi:
		{
			auto str = QStringLiteral("// ") + QString::number(sz.width()) + QStringLiteral(", ") + QString::number(sz.height()) + QStringLiteral("\n");
			ts.data.append(str);
			ts.mask.append(str);
			auto nb = 0;
			for(auto& frame : frames)
			{
				str = QStringLiteral("// ") + QString::number(nb) + QStringLiteral("\n");
				ts.data.append(str);
				ts.mask.append(str);
				funcWriteData(ts.data, frame.data, false);
				funcWriteData(ts.mask, frame.mask, false);
				++nb;
			}
		}
		break;

	case ArdBitmap_drawCompressed:
		{
			if((sz.width() > 128) || (sz.height() > 64))
			{
				ts.data.append(tr("Image too big"));
				break;
			}
		}
	case Arduboy2_drawBitmap:
		{
			auto str = QStringLiteral("unsigned char const ");
			str += imgName;
			str += QStringLiteral("[] PROGMEM =\n{\n");
			ts.data.append(str);
			str = QStringLiteral("  // ") + QString::number(sz.width()) + QStringLiteral(", ") + QString::number(sz.height()) + QStringLiteral("\n");
			ts.data.append(str);
			funcWriteData(ts.data, frm.data, true);
			ts.data.append(QStringLiteral("};\n"));
		}
		break;

	case Arduboy2_drawCompressed:
		{
			if((sz.width() > 255) || (sz.height() > 255))
			{
				ts.data.append(tr("Image too big"));
				break;
			}
			auto str = QStringLiteral("unsigned char const ");
			str += imgName;
			str += QStringLiteral("[] PROGMEM =\n{\n  ");
			str += QString::number(sz.width()-1) + QStringLiteral(", ") + QString::number(sz.height()-1) + QStringLiteral(",\n");
			ts.data.append(str);
			funcWriteData(ts.data, frm.data, true);
			ts.data.append(QStringLiteral("};\n"));
		}
		break;

	case Sprites_drawExternalMask:
		{
			if((sz.width() > 255) || (sz.height() > 255))
			{
				ts.data.append("Image too big");
				break;
			}
			// data
			auto str = QStringLiteral("unsigned char const ");
			str += imgName;
			str += QStringLiteral("[] PROGMEM =\n{\n  ");
			str += QString::number(sz.width()) + QStringLiteral(", ") + QString::number(sz.height()) + QStringLiteral(",\n");
			ts.data.append(str);
			// mask
			str = QStringLiteral("unsigned char const ");
			str += imgName;
			str += QStringLiteral("_mask[] PROGMEM =\n{\n  ");
			str += QString::number(sz.width()) + QStringLiteral(", ") + QString::number(sz.height()) + QStringLiteral(",\n");
			ts.mask.append(str);

			auto nb = size_t{0};
			for(auto& frame : frames)
			{
				str = QStringLiteral("  // ") + QString::number(nb) + QStringLiteral("\n");
				ts.data.append(str);
				ts.mask.append(str);
				funcWriteData(ts.data, frame.data, true);
				funcWriteData(ts.mask, frame.mask, true);
				++nb;
			}
			ts.data.append(QStringLiteral("};\n"));
			ts.mask.append(QStringLiteral("};\n"));
		}
		break;

	case Sprites_drawOverwrite:
	case Sprites_drawErase:
	case Sprites_drawSelfMasked:
		{
			if((sz.width() > 255) || (sz.height() > 255))
			{
				ts.data.append(tr("Image too big"));
				break;
			}
			// data
			auto str = QStringLiteral("unsigned char const ");
			str += imgName;
			str += QStringLiteral("[] PROGMEM =\n{\n  ");
			str += QString::number(sz.width()) + QStringLiteral(", ") + QString::number(sz.height()) + QStringLiteral(",\n");
			ts.data.append(str);
			auto nb = 0;
			for(auto& frame : frames)
			{
				str = QStringLiteral("  // ") + QString::number(nb) + QStringLiteral("\n");
				ts.data.append(str);
				funcWriteData(ts.data, frame.data, true);
				++nb;
			}
			ts.data.append(QStringLiteral("};\n"));
		}
		break;
	}
	return ts;
}

void WindowMain::_export(std::vector<QString> const& srcFilepaths, std::vector<QString> const& dstFilepaths)
{
	auto nb = static_cast<int32_t>(srcFilepaths.size());
	for(auto n = 0; n < nb; ++n)
	{
		auto srcFilepath = srcFilepaths[static_cast<size_t>(n)];
		auto dstFilepath = dstFilepaths[static_cast<size_t>(n)];
		QImageReader ir{srcFilepath};
		auto frames = _readImage(ir);
		_convertImages(frames);
		_compressDatas(frames);
		QFile file{dstFilepath};
		if(file.open(QIODevice::WriteOnly|QIODevice::Text) == false)
			continue;
		QTextStream out{&file};
		out << QStringLiteral("#pragma once\n\n");
		auto ts = _toString(frames, 0, static_cast<eFormat>(_ui.formatCombo->currentIndex()), QFileInfo{srcFilepath}.baseName());
		out << ts.data << QStringLiteral("\n") << ts.mask;
	}
}

void WindowMain::_exportAs(std::vector<QString> const& srcFilepaths, QString const& dstFilepath)
{
	QFile file{dstFilepath};
	if(file.open(QIODevice::WriteOnly|QIODevice::Text) == false)
		return;
	QTextStream out{&file};
	out << QStringLiteral("#pragma once\n\n");

	for(auto& srcFilepath : srcFilepaths)
	{
		QImageReader ir{srcFilepath};
		auto frames = _readImage(ir);
		_convertImages(frames);
		_compressDatas(frames);
		auto ts = _toString(frames, 0, static_cast<eFormat>(_ui.formatCombo->currentIndex()), QFileInfo{srcFilepath}.baseName());
		out << QStringLiteral("//") << srcFilepath << QStringLiteral("\n");
		out << ts.data << QStringLiteral("\n") << ts.mask;
	}
}

void WindowMain::_displayData()
{
	auto ts = _toString(_curFrames, _curFrameIdx, static_cast<eFormat>(_ui.formatCombo->currentIndex()), QFileInfo{_filepath}.baseName());
	_ui.imageData->clear();
	_ui.imageData->appendPlainText(ts.data);
	_ui.imageMask->clear();
	_ui.imageMask->appendPlainText(ts.mask);


	// status bar
	auto& frm = _curFrames[_curFrameIdx];
	auto dataSize = frm.data.size();
	auto dataPercent = 100.0f - static_cast<float>(frm.data.size()) / static_cast<float>(frm.noCompressDataSize) * 100.0f;
	auto maskSize = frm.mask.size();

	auto sz = frm.img.size();
	auto str = QStringLiteral("Image: ") + QString::number(sz.width()) + QStringLiteral("x") + QString::number(sz.height()) + QStringLiteral(" - ");
	if(_ui.formatCombo->currentIndex() == 0)
	{
		str += QStringLiteral("Data: ") + QString::number(dataSize) + QStringLiteral(" B - ");
		str += QStringLiteral("Mask: ") + QString::number(maskSize) + QStringLiteral(" B");
	}
	else
	{
		str += QStringLiteral("Data: ") + QString::number(dataSize) + QStringLiteral(" B (") + QString::number(static_cast<double>(dataPercent), 'g', 3) + QStringLiteral("%)");
	}
	_ui.dataInfo->setText(str);
}

void WindowMain::_resetParams()
{
	_imageNegative = false;
	_ui.imageView->resetScale();
	_imageSlice = SliceInfo{};
}
