#include "window_main.h"
#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication app{argc, argv};
    app.setWindowIcon(QIcon{":/icon_app.png"});
    WindowMain window;
    window.show();
    return app.exec();
}
