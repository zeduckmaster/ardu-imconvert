QT += core gui widgets
TARGET = ardu-imconvert
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
src/main.cpp \
src/window_main.cpp

HEADERS  += \
src/window_main.h

RESOURCES += \
src/res.qrc

win32 {
RC_ICONS += src/icon_app.ico
}

macx {
ICON = src/icon_app.icns
}


